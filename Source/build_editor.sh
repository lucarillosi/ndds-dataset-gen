#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# UE4_HOME="$DIR/../../UE4_Base/"
UE4_HOME="/data/thirdparty/UnrealEngine"
$UE4_HOME/Engine/Build/BatchFiles/Linux/Build.sh NDDSEditor Linux Development $DIR/NDDS.uproject -waitmutex $@